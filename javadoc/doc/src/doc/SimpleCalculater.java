package doc;
/**
 * To perform basic calculator functions
 * @author Vinoja
 * @since 17/05/2020
 * @version 1.0.0v
 * 
 *
 */
public class SimpleCalculater {
	/**
	 * variable a,number 1 value
	 */
	 public int a;
	 
	 /**
	  * variable b,number 2 value
	  */
	  public int b;
	  /**
	   * to add two integer numbers.
	   * will return sum of given two integers(a+b)
	   * 
	   * @param a adding first value
	   * @param b adding first value
	   * @return (a+b)
	  */
	  public int sum(int a,int b) {
		  return a+b;
		  
	  }
	   /**
	    * to subtract two integer number
	    * will return sum of given two integers(a+b)
	    * 
	    * @param a adding first value
	    * @param b adding first value
	    * @return (a-b)
	    * 
	    */
	   public int sub(int a,int b) {
			  return a-b;
	   }  
			/*
			 * this is the main method of this program
			 *  @param args
			 *  */
	   
	  public static void main(String[] args ) {
		  SimpleCalculater cal=new SimpleCalculater();
		  System.out.println(cal.sub(50,30));
	  }
	 
	 
}

