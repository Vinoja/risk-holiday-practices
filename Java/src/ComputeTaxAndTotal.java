import java.util.Scanner;
public class ComputeTaxAndTotal {
	public static void main(String args[]) {
	Scanner input=new Scanner(System.in);
	System.out.print("Enter SubTotal:");
		int subTotal=input.nextInt();
		System.out.print("Enter TaxPercent:");
		double taxPercent=input.nextDouble();
		
		int tax=(int)(subTotal*taxPercent/100);
		int total=(subTotal+tax);
		
		System.out.println("The subtotal = "+subTotal/100+" dollars and "+subTotal%100+" cents.");
		System.out.println("The tax rate = "+ taxPercent +" percent.");
		System.out.println("The tax = "+tax/100+" dollars and "+tax%100+" cents.");
		System.out.println("The total = "+ total/100+" dollars and "+ total%100+" cents.");
		}
	
}