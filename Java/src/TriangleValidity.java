public class TriangleValidity {
	public static void main(String args[]) {
		
		double sideA=32.5; double sideB=51.6; double sideC=20.6;

		boolean valid= isValidTriangle(sideA, sideB, sideC);
		if(valid) {
			System.out.println("it is valid triangle");
		}else {
			System.out.println("it is not valid triangle");
		}
	}

	public static boolean isValidTriangle(double sideA, double sideB, double sideC) {

		double ab = sideA + sideB;
		double ac = sideA + sideC;
		double bc = sideB + sideC;

		if ((sideA < bc && sideB < ac && sideC < ab)) {
			return true;
		}
		return false;
	}
}



